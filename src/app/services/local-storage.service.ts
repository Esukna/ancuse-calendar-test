import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/internal/Observable';
import {from} from 'rxjs';
import {filter, map, toArray} from 'rxjs/operators';
import {ScheduleEvent} from '../intefaces/schedule';


@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  key = 'calendarData';
  localst$: Observable<any>;

  constructor() {
  }

  /**
   * Return observable of Localstorage
   */
  public getLocalStorage$(): Observable<any> {
    return this.localst$ = from(JSON.parse(localStorage.getItem(this.key) || '[]'));
  }

  /**
   * Set transfered data in localstorage
   */
  public setInLocalStorage(eventList): void {
    localStorage.setItem(this.key, JSON.stringify(eventList));
  }

  /**
   * Observable filter data and return events which belong transfered day
   */
  public filterEventOnDay(num: any): Observable<any> {
    return this.getLocalStorage$().pipe(
      filter((item: ScheduleEvent) => {
        return item.day === num;
      }),
      toArray()
    );
  }

  /**
   * Delete from localstorage event by id and return selected date filtered events
   */
  public delByIdFromLocalstorage(momentDay: any, id: number): Observable<any> {
    this.getLocalStorage$().pipe(
      filter((item) => item.id !== id),
      toArray()
    ).subscribe((res) => {
      this.setInLocalStorage(res);
    });
    return this.filterEventOnDay(momentDay);
  }

  /**
   * Return Observable with edited events
   */
  public editByIdFromLocalstorage(event, id): Observable<any> {
    return this.getLocalStorage$().pipe(
      map((item) => {
        if (item.id === id) {
          item.title = event.form.value.title;
          item.description = event.form.value.description;
        }
        return item;
      }),
      toArray()
    );
  }

}
