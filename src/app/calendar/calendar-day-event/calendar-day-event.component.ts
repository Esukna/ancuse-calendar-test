import {Component, EventEmitter, Input, OnDestroy, Output} from '@angular/core';
import {LocalStorageService} from '../../services/local-storage.service';
import {SubscriptionLike} from 'rxjs/internal/types';

declare var moment: any;

@Component({
  selector: 'app-calendar-day-event',
  templateUrl: './calendar-day-event.component.html',
  styleUrls: ['./calendar-day-event.component.sass']
})
export class CalendarDayEventComponent implements OnDestroy {

  @Input() item;
  @Input() momentDate;
  @Output() emitDeleted = new EventEmitter<any>();
  @Output() emitShowModal = new EventEmitter<any>();

  subscriptions: SubscriptionLike[] = [];

  constructor(public ls: LocalStorageService
  ) { }

  /**
   * Show time created
   */
  public getTimeEventItem(item): any {
    const momentTime = moment(item.timepick).format('HH:mm');
    return momentTime;
  }

  /**
   * Delete event by click by id
   */
  public deleteEvent(id: number): void {
    this.subscriptions.push(this.ls.delByIdFromLocalstorage(this.momentDate, id).subscribe((res) => {
      this.emitDeleted.emit({
        eventsAfterDeleting: res, id
      });
    }));
  }

  /**
   * Show edited modal by click edit
   */
  public showEditModal(item): void {
    this.emitShowModal.emit({
      editedItem: item
    });
  }

  ngOnDestroy(): void {
    if (this.subscriptions) {
      this.subscriptions.forEach( (subscription) => {
          if (subscription) {
            subscription.unsubscribe();
          }
        }
      );
      this.subscriptions = [];
    }
  }

}
