import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {LocalStorageService} from '../../services/local-storage.service';
import {toArray} from 'rxjs/operators';
import {Schedule} from '../../intefaces/schedule';
import {NzMessageService} from 'ng-zorro-antd';
import {EditModalComponent} from '../edit-modal/edit-modal.component';
import {SubscriptionLike} from 'rxjs/internal/types';

declare var moment: any;

@Component({
  selector: 'app-calendar-day',
  templateUrl: './calendar-day.component.html',
  styleUrls: ['./calendar-day.component.sass']
})
export class CalendarDayComponent implements OnInit, OnDestroy {
  currentLocalStorage = [];
  momentDate: string;
  num;
  available = false;
  filteredEventShow = [];
  celebration: any;
  @ViewChild('editModal', {static: true}) editModal: EditModalComponent;

  subscriptions: SubscriptionLike[] = [];

  scheduleData: Schedule[] = [
    {time: moment('00-00', 'hh-mm').format('HH'), event: [], availableHour: false},
    {time: moment('01-00', 'hh-mm').format('HH'), event: [], availableHour: false},
    {time: moment('02-00', 'hh-mm').format('HH'), event: [], availableHour: false},
    {time: moment('03-00', 'hh-mm').format('HH'), event: [], availableHour: false},
    {time: moment('04-00', 'hh-mm').format('HH'), event: [], availableHour: false},
    {time: moment('05-00', 'hh-mm').format('HH'), event: [], availableHour: false},
    {time: moment('06-00', 'hh-mm').format('HH'), event: [], availableHour: false},
    {time: moment('07-00', 'hh-mm').format('HH'), event: [], availableHour: false},
    {time: moment('08-00', 'hh-mm').format('HH'), event: [], availableHour: false},
    {time: moment('09-00', 'hh-mm').format('HH'), event: [], availableHour: false},
    {time: moment('10-00', 'hh-mm').format('HH'), event: [], availableHour: false},
    {time: moment('11-00', 'hh-mm').format('HH'), event: [], availableHour: false},
    {time: moment('12-00', 'hh-mm').format('HH'), event: [], availableHour: false},
    {time: moment('13-00', 'hh-mm').format('HH'), event: [], availableHour: false},
    {time: moment('14-00', 'hh-mm').format('HH'), event: [], availableHour: false},
    {time: moment('15-00', 'hh-mm').format('HH'), event: [], availableHour: false},
    {time: moment('16-00', 'hh-mm').format('HH'), event: [], availableHour: false},
    {time: moment('17-00', 'hh-mm').format('HH'), event: [], availableHour: false},
    {time: moment('18-00', 'hh-mm').format('HH'), event: [], availableHour: false},
    {time: moment('19-00', 'hh-mm').format('HH'), event: [], availableHour: false},
    {time: moment('20-00', 'hh-mm').format('HH'), event: [], availableHour: false},
    {time: moment('21-00', 'hh-mm').format('HH'), event: [], availableHour: false},
    {time: moment('22-00', 'hh-mm').format('HH'), event: [], availableHour: false},
    {time: moment('23-00', 'hh-mm').format('HH'), event: [], availableHour: false},
  ];

  constructor(private route: ActivatedRoute,
              public ls: LocalStorageService,
              private message: NzMessageService) {
  }

  ngOnInit() {
    // get localstorage and set to two way binding variable -> 'currentLocalStorage'
    this.subscriptions.push(this.ls.getLocalStorage$()
      .pipe(
        toArray()
      ).subscribe((res) => {
      this.currentLocalStorage = res;
    }));

    // set in local variables query param
    this.subscriptions.push(this.route.queryParams.subscribe((params) => {
      this.momentDate = params.momentDate;
      this.available = params.available === 'true';
      this.celebration = params.celebration;
    }));

    // set to local variable -> 'num' parameter from query ( num - number of day in month )
    this.subscriptions.push(this.route.params.subscribe((params) => {
      this.num = params.num;
    }));
    // filter events only on current day
    this.filtered();
    // put the right event on the right day
    this.concatEventInDate();
  }

  /**
   * When submit add event set in locastorage and update local variables state
   */
  submitForm(event) {
    this.currentLocalStorage = event.currentLocalStorage;
    this.currentLocalStorage.push(event.value);
    this.ls.setInLocalStorage(this.currentLocalStorage);
    this.filtered();
    this.concatEventInDate();
  }

  /**
   * Check if have min 5 word and hint how much is left
   */
  checkWordCount(control) {
    if (control.value !== null) {
      const countWord = control.value.split(' ').length;
      if (countWord < 5) {
        return {countWord: 5 - countWord};
      } else {
        return null;
      }
    }
  }

  /**
   * Show selected date in format
   */
  showSelectedDate() {
    return moment(this.momentDate, 'DD-MM-YYYY').format('MMMM DD, YYYY');
  }

  /**
   * Show name of weekend in format
   */
  showSelectedNameDate() {
    return moment(this.momentDate, 'DD-MM-YYYY').format('dddd');
  }

  /**
   * Create pop up info if havent events on selected day
   */
  createBasicMessage(): void {
    this.message.info('You have not event on this day!');
  }

  /**
   * When click on delete, event in child component
   * return event after deleting and refresh 'scheduleData' variable
   */
  emitDeleted(event) {
    this.filteredEventShow = event.eventsAfterDeleting;
    this.deleteEventInDate(event.id);
  }

  /**
   * Util helper for fn -> emitDeleted
   * refresh variable 'scheduleData'
   */
  deleteEventInDate(id) {
    this.scheduleData.forEach((date) => {
      const index = date.event.findIndex((item) => {
        return item.id === id;
      });
      if (index !== -1) {
        date.event.splice(index, 1);
      }
    });
  }

  /**
   * set inside scheduleData item new data event
   */
  concatEventInDate() {
    this.scheduleData.forEach((date) => {
      this.compareTime(date);
    });
  }

  /**
   * Compare time from 'scheduleData' and 'filteredEventShow'
   * if data equally set event in grid 'scheduleData'
   */
  compareTime(date) {
    const filteredTimeMap = this.filteredEventShow.filter((event) => {
      const eventHour = moment(event.timepick).format('HH');
      if (date.time === eventHour) {
        return event;
      }
    });
    if (filteredTimeMap.length > 0) {
      date.event = filteredTimeMap;
    }
  }

  /**
   * Show modal for edit event
   */
  showEditModal(event) {
    this.editModal.showEditModal(event.editedItem);
  }

  /**
   * Show events only on a selected day
   * if havent events on selected day show info
   */
  filtered() {
    this.subscriptions.push(this.ls.filterEventOnDay(this.momentDate).subscribe((res) => {
      if (res.length > 0) {
        this.filteredEventShow = res;
      } else {
        this.createBasicMessage();
      }
    }));
  }

  /**
   * When emitted edit refresh grid and local variable
   */
  editEvent() {
    this.filtered();
    this.concatEventInDate();
  }

  ngOnDestroy(): void {
    if (this.subscriptions) {
      this.subscriptions.forEach( (subscription) => {
          if (subscription) {
            subscription.unsubscribe();
          }
        }
      );
      this.subscriptions = [];
    }
  }


}
