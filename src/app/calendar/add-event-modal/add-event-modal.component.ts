import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {toArray} from 'rxjs/operators';
import {ScheduleEvent} from '../../intefaces/schedule';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {LocalStorageService} from '../../services/local-storage.service';
import {SubscriptionLike} from 'rxjs/internal/types';

declare var moment: any;

@Component({
  selector: 'app-add-event-modal',
  templateUrl: './add-event-modal.component.html',
  styleUrls: ['./add-event-modal.component.sass']
})
export class AddEventModalComponent implements OnInit, OnDestroy {

  validateForm: FormGroup;
  isVisible = false;
  time = new Date();

  @Input() selectMoment: any;
  @Input() checkWordCount: any;
  @Input() available: any;
  @Input() momentDate: any;
  @Output() currentLocalStorage = new EventEmitter<any>();

  subscriptions: SubscriptionLike[] = [];

  constructor(private ls: LocalStorageService) {
  }

  ngOnInit() {
    // create validate Form
    this.validateForm = new FormGroup({
      title: new FormControl('', Validators.required),
      description: new FormControl('', [this.checkWordCount.bind(this), Validators.required]),
      timePick: new FormControl('', [this.checkTimeAvailable.bind(this), Validators.required]),
    });
  }

  /**
   * Check if time is past or not
   */
  private checkTimeAvailable(): null | { before: boolean } {
    const isDayAfter = moment(this.momentDate, 'DD-MM-YYYY').isAfter(moment(), 'day');
    if (isDayAfter) {
      return null;
    }
    const isBefore = moment(this.time).isBefore(moment(), 'minute');
    if (isBefore) {
      return {before: true};
    } else {
      return null;
    }
  }

  /**
   * Submit function
   * take submited data and push in localstorage, clear form, and close it
   */
  submitForm() {
    const value: ScheduleEvent = {
      title: this.validateForm.controls.title.value, // заголовок
      day: this.selectMoment, // дата ( число которое открыто )
      timepick: moment(this.validateForm.controls.timePick.value), // time which choose user
      description: this.validateForm.controls.description.value,
      id: new Date().getUTCMilliseconds() // impovization uniq id ( One in 999 case CAN NOT TO BE UNIQ !!! )
    };
    this.subscriptions.push(this.ls.getLocalStorage$().pipe(
      toArray()
    ).subscribe((res) => {
      this.currentLocalStorage.emit({
        currentLocalStorage: res,
        value
      });
    }));
    this.validateForm.reset();
    this.handleOk();
  }

  /**
   * Show modal
   */
  public showModal(): void {
    this.isVisible = true;
  }

  /**
   * Close modal when submit
   */
  public handleOk(): void {
    this.isVisible = false;
  }

  /**
   * Close modal when cancel modal
   */
  public handleCancel(): void {
    this.isVisible = false;
  }

  ngOnDestroy(): void {
    if (this.subscriptions) {
      this.subscriptions.forEach( (subscription) => {
          if (subscription) {
            subscription.unsubscribe();
          }
        }
      );
      this.subscriptions = [];
    }
  }

}
