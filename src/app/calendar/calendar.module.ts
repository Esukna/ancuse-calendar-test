import {NgModule} from '@angular/core';
import {CalendarMonthComponent} from './calendar-month/calendar-month.component';
import {CalendarDayComponent} from './calendar-day/calendar-day.component';
import {RouterModule} from '@angular/router';
import {SharedModule} from '../shared/shared.module';
import {CommonModule} from '@angular/common';
import {EditModalComponent} from './edit-modal/edit-modal.component';
import {AddEventModalComponent} from './add-event-modal/add-event-modal.component';
import { CalendarDayEventComponent } from './calendar-day-event/calendar-day-event.component';

@NgModule({
  declarations: [
    CalendarMonthComponent,
    CalendarDayComponent,
    EditModalComponent,
    AddEventModalComponent,
    CalendarDayEventComponent],
  imports: [
    RouterModule,
    CommonModule,
    SharedModule
  ],
  providers: []
})
export class CalendarModule {
}
