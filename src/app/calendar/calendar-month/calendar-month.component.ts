import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {LocalStorageService} from '../../services/local-storage.service';
import {HolidayService} from '../../services/holiday.service';
import {SubscriptionLike} from 'rxjs/internal/types';
declare var moment: any;

@Component({
  selector: 'app-calendar-month',
  templateUrl: './calendar-month.component.html',
  styleUrls: ['./calendar-month.component.sass']
})
export class CalendarMonthComponent implements OnInit, OnDestroy {

  locale = 'en-GB';
  navDate: any;
  grid: Array<any> = [];
  currentLocalStorage: Array<any> = [];
  weekDaysHeader: Array<string> = [];

  subscriptions: SubscriptionLike[] = [];

  constructor(private router: Router,
              private localStorageService: LocalStorageService,
              private holidayService: HolidayService) {
  }

  ngOnInit() {
    // set locale
    moment.locale(this.locale);
    // set holiday
    this.holidayService.createHoliday();
    // all day in current month
    this.navDate = moment();
    // get data from localstorage and set to two way binding variable "currentLocalStorage"
    this.subscriptions.push(this.localStorageService.getLocalStorage$().subscribe((res) => {
      this.currentLocalStorage = res;
    }));
    // create calendar header
    this.makeHeader();
    // create calendar grid
    this.makeGrid();
  }

  /**
   * Create Header
   */
  private makeHeader(): void {
    const weekDaysArr: Array<number> = [0, 1, 2, 3, 4, 5, 6];
    weekDaysArr.forEach(day => this.weekDaysHeader.push(moment().weekday(day).format('ddd')));
  }

  /**
   * Create grid and set data on each day
   */
  private makeGrid(): void {
    this.grid = [];
    const firstDayDate = moment(this.navDate).startOf('month');
    const initialEmptyCells = firstDayDate.weekday();
    const lastDayDate = moment(this.navDate).endOf('month');
    const lastEmptyCells = 6 - lastDayDate.weekday();
    const daysInMonth = this.navDate.daysInMonth();
    const arrayLength = initialEmptyCells + lastEmptyCells + daysInMonth;

    for (let i = 0; i < arrayLength; i++) {
      const obj: any = {};
      if (i < initialEmptyCells || i > initialEmptyCells + daysInMonth - 1) {
        obj.value = 0;
        obj.available = false;
      } else {
        const numberOfDay = i - initialEmptyCells + 1;
        obj.value = numberOfDay;
        obj.available = this.isAvailable(numberOfDay);
        obj.weekends = this.isWeekendsDay(numberOfDay);
        obj.today = this.isToday(numberOfDay);
        // set number of events
        const dayMoment = moment(this.dateFromNum(numberOfDay, this.navDate)).format('DD-MM-YYYY');
        this.subscriptions.push(this.localStorageService.filterEventOnDay(dayMoment).subscribe((res) => {
            obj.events = res.length;
        }));
        obj.holiday = moment(this.dateFromNum(numberOfDay, this.navDate)).isHoliday();
      }
      this.grid.push(obj);
    }
  }

  /**
   * Is today transfered date
   */
  private isToday(date: number): boolean {
    return moment().isSame(this.dateFromNum(date, this.navDate), 'day');
  }

  /**
   * Is weekends transfered day
   */
  public isWeekendsDay(day): boolean {
    const weekendDay = moment(`${day}-${this.navDate.month() + 1}-${this.navDate.year()}`, 'DD-MM-YYYY').format('ddd');
    return weekendDay === 'Sat' || weekendDay === 'Sun';
  }

  /**
   * decide if date past
   */
  private isAvailable(num: number): boolean {
    if (this.isAvailableLogic) {
      const dateToCheck = this.dateFromNum(num, this.navDate);
      return this.isAvailableLogic(dateToCheck);
    } else {
      return true;
    }
  }

  /**
   * Return date of transfered date number and month
   */
  private dateFromNum(num: number, referenceDate: any): any {
    const returnDate = moment(referenceDate);
    return returnDate.date(num);
  }

  /**
   * Utility method of fn -> 'isAvailable'
   * decide if transfered date was before current moment
   */
  private isAvailableLogic(dateToCheck: any): boolean {
    return !dateToCheck.isBefore(moment(), 'day');
  }

  /**
   * Change month in calendar
   */
  public changeNavMonth(num: number): void {
    if (this.canChangeNavMonth(num)) {
      this.navDate.add(num, 'month');
      this.makeGrid();
    }
  }

  /**
   * Utility method of fn -> 'changeNavMonth'
   * say can we change month ( no set limit )
   */
  private canChangeNavMonth(num: number): boolean {
    const clonedDate = moment(this.navDate);
    return this.canChangeMonthLogic(num, clonedDate);
  }

  /**
   * Utility method of fn -> 'changeNavMonth'
   * Set min and max calendar
   */
  private canChangeMonthLogic(num: number, currentDate: any): boolean {
    currentDate.add(num, 'month');
    const minDate = moment().add(-1, 'month');
    const maxDate = moment().add(1, 'year');
    return currentDate.isBetween(minDate, maxDate);
  }

  // we can do it throw storage service, but this way better cause not much parameters
  /**
   * Open day and set query parameters
   */
  public openDayInfo(day: number, available: boolean): void {
    if (day !== 0 ) {
      const findMomentDate = this.dateFromNum(day, this.navDate);
      this.router.navigate(['/day/', day], {queryParams: {
        momentDate: findMomentDate.format('DD-MM-YYYY'),
        available,
        celebration: moment(findMomentDate).isHoliday()
      }});
    }
  }

  ngOnDestroy(): void {
    if (this.subscriptions) {
      this.subscriptions.forEach( (subscription) => {
          if (subscription) {
            subscription.unsubscribe();
          }
        }
      );
      this.subscriptions = [];
    }
  }
}
