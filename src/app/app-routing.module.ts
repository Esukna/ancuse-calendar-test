import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {WildcardComponent} from './wildcard/wildcard.component';
import {CalendarMonthComponent} from './calendar/calendar-month/calendar-month.component';
import {CalendarDayComponent} from './calendar/calendar-day/calendar-day.component';

const routes: Routes = [
  {
    path: '',
    component: CalendarMonthComponent,
    data: {
      depth: 1
    }
  },
  {
    path: 'day/:num',
    component: CalendarDayComponent,
    data: {
      depth: 2
    }
  },
  {
    path: '**',
    component: WildcardComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
