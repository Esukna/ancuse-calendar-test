import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {en_GB, NZ_I18N} from 'ng-zorro-antd';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {WildcardComponent} from './wildcard/wildcard.component';
import {CalendarModule} from './calendar/calendar.module';
import {registerLocaleData} from '@angular/common';
import en from '@angular/common/locales/en-GB';
import {SharedModule} from './shared/shared.module';

registerLocaleData(en);

@NgModule({
  declarations: [
    AppComponent,
    WildcardComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    CalendarModule,
    SharedModule
  ],
  providers: [
    {provide: NZ_I18N, useValue: en_GB}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
