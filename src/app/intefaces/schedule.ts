export interface Schedule {
  time: string;
  event: ScheduleEvent[];
  availableHour: boolean;
}

export interface ScheduleEvent {
  id: number;
  title: string;
  day: any;
  timepick: any;
  description: string;
}
